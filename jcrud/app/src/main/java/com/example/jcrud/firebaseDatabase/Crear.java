package com.example.jcrud.firebaseDatabase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.jcrud.CallBacks.OnValueBoolean;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

public class Crear {
    public Crear() {
    }
    public void CrearJ(String url, JSONObject jsonObject, OnValueBoolean cbool){
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference(url);
        //String keyz= reference.getKey();//crea una clave automatica,solo soirve con inicio de secion
        HashMap<String, Object> hashMap= new HashMap<>();
        Iterator<String> iterator=jsonObject.keys();
        while(iterator.hasNext()){
            try{
                String key=iterator.next();
                Object value=jsonObject.get(key);
                hashMap.put(key, value);
        }catch (Exception e){
                e.printStackTrace();
            }
        }
        //cuando pasamos valor defecto, no se hace el key ultimate
        reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                cbool.OnResponseBool(task.isComplete());
                Log.e("TAG", "onDataChange: "+ task.isComplete());

            }
        });

    }
}
