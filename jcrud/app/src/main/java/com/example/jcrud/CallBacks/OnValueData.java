package com.example.jcrud.CallBacks;

import com.google.firebase.database.DataSnapshot;

public interface OnValueData {
    void OnResponseData(DataSnapshot snapshot);
}
