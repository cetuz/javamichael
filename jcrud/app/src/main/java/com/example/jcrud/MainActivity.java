package com.example.jcrud;

import static com.example.jcrud.Constantes.IS_CHHILD_USUARIOS;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jcrud.CallBacks.OnValueBoolean;
import com.example.jcrud.CallBacks.OnValueData;
import com.example.jcrud.Model.Usuarios;
import com.example.jcrud.firebaseDatabase.Consultar;
import com.example.jcrud.firebaseDatabase.Crear;
import com.example.jcrud.firebaseDatabase.Editar;
import com.example.jcrud.firebaseDatabase.Eliminar;
import com.google.firebase.database.DataSnapshot;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    TextView tvProvar;
    EditText etNombre,etApellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnProvar=findViewById(R.id.btnProvar);
        Button btnCrear=findViewById(R.id.btnCrear);
        Button btneditar=findViewById(R.id.btneditar);
        Button btneliminar=findViewById(R.id.btneliminar);
        tvProvar=findViewById(R.id.tvProvar);
        etNombre=findViewById(R.id.etNombre);
        etApellido=findViewById(R.id.etApellido);


        btnProvar.setOnClickListener(v-> Mostrar());
        btnCrear.setOnClickListener(v-> Create());
        btneditar.setOnClickListener(v-> Editado());
        btneliminar.setOnClickListener(v-> Delate());
    }

    private void Delate(){
        String url=String.format("%s/%s",IS_CHHILD_USUARIOS,"a2");
        new Eliminar().EliminarJ(url, new OnValueBoolean() {
            @Override
            public void OnResponseBool(Boolean bool) {
                if(bool){
                    Toast.makeText(MainActivity.this,"eliminado correctamente",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(MainActivity.this,"error al eliminar",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private String Transor(EditText editText){
        return editText.getText().toString().trim();
    }

    private  void Editado(){
        String url=String.format("%s/%s",IS_CHHILD_USUARIOS,"a2");
        JSONObject json= new JSONObject();
        try {
            json.put("Nombre",Transor(etNombre));//en el jason toma el campo, asi que no se declara en la url
            json.put("Apellido",Transor(etApellido));
            new Editar().EditarJ(url, json, new OnValueBoolean() {
                @Override
                public void OnResponseBool(Boolean bool) {
                    if(bool){
                        Toast.makeText(MainActivity.this,"editado correctamente",Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(MainActivity.this,"error al editar",Toast.LENGTH_LONG).show();
                    }
                }
            });

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void Create(){
        SimpleDateFormat formatEntrada = new SimpleDateFormat("ddMMyyyykkmmssS");
        Date fechaEntrada = new Date();
        String fecha = formatEntrada.format(fechaEntrada).toString();


        String url=String.format("%s/%s/",IS_CHHILD_USUARIOS,fecha);
        JSONObject json= new JSONObject();
        try {
            json.put("Nombre",Transor(etNombre));
            json.put("Apellido",Transor(etApellido));
            new Crear().CrearJ(url, json, new OnValueBoolean() {
                @Override
                public void OnResponseBool(Boolean bool) {
                    if(bool){
                        Toast.makeText(MainActivity.this,"creado correctamente",Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(MainActivity.this,"error al crear",Toast.LENGTH_LONG).show();
                    }
                }
            });

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void Mostrar(){
        String url=String.format("%s/",IS_CHHILD_USUARIOS);
        new Consultar().ConsultarFirebase(url,new OnValueData() {
            @Override
            public void OnResponseData(DataSnapshot snapshot) {
                //tvProvar.setText("snapshot");

                for (DataSnapshot s:snapshot.getChildren())//getchildre, pasa los valores de snap  los cuenta
                    {
                        Usuarios usuarios = s.getValue(Usuarios.class);
                        String data= "<b>Nombre:</b>"+usuarios.getNombre()+" "+"<b>Apellido:</b>"+usuarios.getApellido()+" "+"<b>Id:</b>"+usuarios.getId();
                        tvProvar.setText(Html.fromHtml(data));
                    }

            }
        });
    }
}