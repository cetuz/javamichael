package com.example.jcrud.firebaseDatabase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.jcrud.CallBacks.OnValueBoolean;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Eliminar {
    public Eliminar() {
    }
    public void EliminarJ(String url, OnValueBoolean cbool){
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference(url);
        reference.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                cbool.OnResponseBool(task.isComplete());
                Log.e("TAG", "onDataChange: "+ task.isComplete());//valor boleano
            }
        });
    }
}
