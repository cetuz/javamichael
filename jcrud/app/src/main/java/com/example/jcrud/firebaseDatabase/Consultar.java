package com.example.jcrud.firebaseDatabase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.jcrud.CallBacks.OnValueData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Consultar {
    public Consultar(){
    }
    public void ConsultarFirebase(String url, OnValueData callback){
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference(url);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                callback.OnResponseData(snapshot);
                Log.e("TAG", "onDataChange: "+ snapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                callback.OnResponseData(null);

            }
        });
    };
}
